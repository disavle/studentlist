package com.example.hw2

import java.sql.RowId
import java.time.LocalDate
import java.util.*

data class Item(
    var name: String,
    var birthday: String,
    var group: String,
    var faculty: String
)
