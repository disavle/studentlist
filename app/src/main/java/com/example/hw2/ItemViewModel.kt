package com.example.hw2

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import java.time.LocalDate
import java.util.*

class ItemViewModel: ViewModel() {
    private var itemList = listOf<Item>(Item("Ivan Ivanov Ivanovich", "20-04-2000","1","FPM"),
        Item("Petr Petrov Petrovich","20-04-2001","2","URFAK"),
        Item("Vasilisa Gen Vadimovna","20-04-2003","4","FPM")
    ).toMutableList()

    private var setOfFaculties = if (itemList.isNotEmpty()) {
        val temp = itemList.map { it.faculty }.toMutableList()
        temp.add("All")
        temp.toSortedSet()
    } else {
        setOf<String>()
    }

    private var filterItems = if (itemList.isNotEmpty()) {
        itemList.filter {
            it.faculty == setOfFaculties.elementAt(currentFacultyIndex)
        }
    } else {
        listOf<Item>().toMutableList()
    }

    var currentIndex = 0

    var currentFilterIndex = 0

    var currentFacultyIndex = 0

    val currentSet: String
        get() = setOfFaculties.elementAt(currentFacultyIndex)

    val isEmpty: Boolean
        get() = itemList.isEmpty()

    val isFacultyEmpty: Boolean
        get() = setOfFaculties.isEmpty()

    val currentName: String?
        get() = filterItems[currentFilterIndex].name
    val currentDate: String?
        get() = filterItems[currentFilterIndex].birthday
    val currentGroup: String?
        get() = filterItems[currentFilterIndex].group
    val currentFaculty: String?
        get() = filterItems[currentFilterIndex].faculty

    fun moveToNext() {
        if (filterItems.isNotEmpty()) {
            currentFilterIndex = (currentFilterIndex + 1) % filterItems.size
            currentIndex = itemList.indexOf(filterItems[currentFilterIndex])
        }
    }

    fun moveToPrev() {
        if (filterItems.isNotEmpty()) {
            currentFilterIndex = (filterItems.size + currentFilterIndex - 1) % filterItems.size
            currentIndex = itemList.indexOf(filterItems[currentFilterIndex])
        }
    }

    fun moveToFNext() {
        if (setOfFaculties.isNotEmpty()) {
            currentFacultyIndex = (currentFacultyIndex + 1) % setOfFaculties.size
            updateFilter()
        }
    }

    fun moveToFPrev() {
        if (setOfFaculties.isNotEmpty()) {
            currentFacultyIndex = (setOfFaculties.size + currentFacultyIndex - 1) % setOfFaculties.size
            updateFilter()
        }
    }

    fun delCurr() {
        if (itemList.isNotEmpty()) {
            itemList.removeAt(currentIndex)
            if (currentIndex > 0){
                currentIndex -= 1
                currentFilterIndex -= 1
            }
            updateSet()
        }
    }

    fun editCurr(name: String, birth: String, group: String, faculty: String) {
        if (itemList.isNotEmpty()) {
            itemList[currentIndex].name = name
            itemList[currentIndex].birthday = birth
            itemList[currentIndex].group = group
            itemList[currentIndex].faculty = faculty
            updateSet()
        }
    }

    fun addItem(name: String, birth: String, group: String, faculty: String) {
        itemList.add(Item(name,birth,group,faculty))
        currentIndex = itemList.size - 1
        currentFacultyIndex = setOfFaculties.size - 1
        updateSet()
    }

    fun updateFilter() {
        if (itemList.isNotEmpty()){
            filterItems = itemList.filter {
                if (setOfFaculties.elementAt(currentFacultyIndex).toString() == "All") {
                    it.faculty.isNotEmpty()
                } else {
                    it.faculty == setOfFaculties.elementAt(currentFacultyIndex)
                }
            }
            currentFilterIndex = 0
            currentIndex = itemList.indexOf(filterItems[currentFilterIndex])
        } else {
            filterItems = listOf<Item>().toMutableList()
            currentFilterIndex = 0
        }
    }

    fun updateSet() {
        if (itemList.isNotEmpty()){
            val temp = itemList.map { it.faculty }.toMutableList()
            temp.add("All")
            setOfFaculties = temp.toSortedSet()
            currentFacultyIndex = 0
        } else {
            setOfFaculties =  setOf<String>()
            currentFacultyIndex = 0
        }
        updateFilter()
    }
}