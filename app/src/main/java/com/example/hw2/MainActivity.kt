package com.example.hw2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.ViewModelProvider
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

private const val TAG = "MainActivity"
private const val KEY_INDEX = "index"

class MainActivity : AppCompatActivity() {

    private lateinit var newBtn: Button
    private lateinit var editBtn: Button
    private lateinit var delBtn: Button
    private lateinit var textViewItem: TextView
    private lateinit var prevBtn: Button
    private lateinit var nextBtn: Button
    private lateinit var textViewFaculty: TextView
    private lateinit var prevFBtn: Button
    private lateinit var nextFBtn: Button

    private val itemViewModel: ItemViewModel by lazy {
        val provider = ViewModelProvider(this)
        provider.get(ItemViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        itemViewModel.currentIndex = savedInstanceState?.getInt(KEY_INDEX)?:0
        itemViewModel.updateFilter()

        newBtn = findViewById(R.id.newBtn)
        editBtn = findViewById(R.id.editBtn)
        delBtn = findViewById(R.id.deleteBtn)
        textViewItem = findViewById(R.id.textViewItem)
        prevBtn = findViewById(R.id.prevBtn)
        nextBtn = findViewById(R.id.nextBtn)
        textViewFaculty = findViewById(R.id.textViewFaculty)
        prevFBtn = findViewById(R.id.prevFBtn)
        nextFBtn = findViewById(R.id.nextFBtn)

        val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                if (data?.getBooleanExtra("newMode", true) ?: true) {
                    itemViewModel.addItem(
                        data?.getStringExtra("newName").toString(),
                        data?.getStringExtra("newDate").toString(),
                        data?.getStringExtra("newGroup").toString(),
                        data?.getStringExtra("newFaculty").toString()
                    )
                } else {
                    itemViewModel.editCurr(
                        data?.getStringExtra("newName").toString(),
                        data?.getStringExtra("newDate").toString(),
                        data?.getStringExtra("newGroup").toString(),
                        data?.getStringExtra("newFaculty").toString()
                    )
                }
                updateItem()
            } else  {
                mess("Changes were not saved")
            }
        }

        newBtn.setOnClickListener {
            val i = Intent(this, EditActivity::class.java)
            i.putExtra("mode",true)
            resultLauncher.launch(i)
        }
        editBtn.setOnClickListener {
            if (itemViewModel.isEmpty) {
                mess("There is nothing to edit")
            } else {
                val i = Intent(this, EditActivity::class.java)
                i.putExtra("mode",false)
                i.putExtra("name",itemViewModel.currentName)
                i.putExtra("date",itemViewModel.currentDate)
                i.putExtra("group",itemViewModel.currentGroup)
                i.putExtra("faculty",itemViewModel.currentFaculty)
                resultLauncher.launch(i)
            }
        }
        delBtn.setOnClickListener {
            if (itemViewModel.isEmpty) {
                mess("List is empty")
            } else {
                itemViewModel.delCurr()
                updateItem()
            }
        }

        nextBtn.setOnClickListener {
            if (itemViewModel.isEmpty) {
                mess("List is empty")
            } else {
                itemViewModel.moveToNext()
                updateItem()
            }
        }
        prevBtn.setOnClickListener {
            if (itemViewModel.isEmpty) {
                mess("List is empty")
            } else {
                itemViewModel.moveToPrev()
                updateItem()
            }
        }

        nextFBtn.setOnClickListener {
            if (itemViewModel.isFacultyEmpty) {
                mess("List is empty")
            } else {
                itemViewModel.moveToFNext()
                updateItem()
            }
        }

        prevFBtn.setOnClickListener {
            if (itemViewModel.isFacultyEmpty) {
                mess("List is empty")
            } else {
                itemViewModel.moveToFPrev()
                updateItem()
            }
        }

        updateItem()
    }

    private fun updateItem(){
        var concatinated = "Empty"
        var concatinatedF = "Empty"
        if (!itemViewModel.isEmpty and !itemViewModel.isFacultyEmpty){
            concatinated = itemViewModel.currentName.toString()
            concatinatedF = itemViewModel.currentSet
        }
        textViewItem.setText(concatinated)
        textViewFaculty.setText(concatinatedF)
    }

    private fun mess(mess: String) {
        Toast.makeText(this, mess, Toast.LENGTH_SHORT)
            .show()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        Log.i(TAG, "onSaveInstanceState")
        savedInstanceState.putInt(KEY_INDEX,itemViewModel.currentIndex)
    }
}