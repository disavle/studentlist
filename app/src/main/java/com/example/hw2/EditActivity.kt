package com.example.hw2

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.time.*
import java.util.*

class EditActivity : AppCompatActivity() {
    private lateinit var editTextName: EditText
    private lateinit var editTextDate: EditText
    private lateinit var editTextGroup: EditText
    private lateinit var editTextFaculty: EditText
    private lateinit var saveBtn: Button
    private var isCreateMode = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        isCreateMode = intent?.getBooleanExtra("mode", true) ?: true

        editTextName=findViewById(R.id.editTextName)
        editTextDate=findViewById(R.id.editTextBirthday)
        editTextGroup=findViewById(R.id.editTextGroup)
        editTextFaculty=findViewById(R.id.editTextTextFaculty)
        saveBtn=findViewById(R.id.saveBtn)

        editTextName.setText(intent.getStringExtra("name"))
        editTextDate.setText(intent.getStringExtra("date"))
        editTextGroup.setText(intent.getStringExtra("group"))
        editTextFaculty.setText(intent.getStringExtra("faculty"))

        if (isCreateMode) {
            saveBtn.text = "Create"
        }

        saveBtn.setOnClickListener {
            if (!editTextName.text.matches(Regex("^[a-zA-ZА-Яа-яЁё\\s]+"))) {
                editTextName.error = "Input name"
            } else
            if (!checkDate(editTextDate.text.toString()) or !editTextDate.text.matches(Regex("\\d{1,2}-\\d{1,2}-\\d{4}"))) {
                editTextDate.error = "Date formate is 'dd-MM-yyyy' or date is not exist or age is not correct"
            } else
            if (!editTextFaculty.text.matches(Regex("^[a-zA-ZА-Яа-яЁё\\s]+"))) {
                editTextFaculty.error = "Input faculty"
            } else {
                save()
            }
        }
    }

    fun save() {
        val data = Intent().apply {
            putExtra("newMode", isCreateMode)
            putExtra("newName", editTextName.text.toString())
            putExtra("newDate", editTextDate.text.toString())
            putExtra("newGroup", editTextGroup.text.toString())
            putExtra("newFaculty", editTextFaculty.text.toString())
        }
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    fun checkDate(dateStr: String): Boolean {
        try {
            var formatter = SimpleDateFormat("dd-MM-yyyy")
            formatter.isLenient = false
            val date = formatter.parse(dateStr)
            val instant: Instant = date.toInstant()
            val zdt: ZonedDateTime = instant.atZone(ZoneId.systemDefault())
            val dateL: LocalDate = zdt.toLocalDate()
            val yo = Period.between(dateL, LocalDate.now()).years
            if (yo >= 100 || yo <= 3) {
                return false
            }
            return true
        } catch (e: Exception) {
            return false
        }
    }

}